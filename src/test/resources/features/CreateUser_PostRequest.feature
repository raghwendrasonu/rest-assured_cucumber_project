Feature: CreateUser_PostRequest
  Scenario: CreateUser_PostRequest
	Given I send POST request with below parameters
	|KEY |VALUE|
	|name|morpheus|
	|job |leader  |
And User hit the webservice https://reqres.in/api/users with post request
Then The status code is 201